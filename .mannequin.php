<?php

/*
 * See: https://mannequin.io/extensions/drupal
 */

use LastCall\Mannequin\Core\MannequinConfig;
use LastCall\Mannequin\Drupal\DrupalExtension;
use Symfony\Component\Finder\Finder;

$theme_name = 'THEME_NAME';
// Assuming this is installed at themes/custom/THEME_NAME
$drupal_dir = __DIR__ . '/../../..';

// Describe where to find Drupal templates.
// See https://symfony.com/doc/current/components/finder.html
$drupalFinder = Finder::create()
  // Templates can live in your normal templates directory.
  ->in(__DIR__ . '/patterns')
  ->files()
  ->name('*.twig');

$drupalExtension = new DrupalExtension([
  'finder'      => $drupalFinder,
  'drupal_root' => $drupal_dir
]);

$drupalExtension->addTwigPath($theme_name, __DIR__ . '/templates');
$drupalExtension->addTwigPath('patterns', __DIR__ . '/patterns');
$drupalExtension->addTwigPath('atoms', __DIR__ . '/patterns/01-atoms');
$drupalExtension->addTwigPath('molecules', __DIR__ . '/patterns/02-molecules');
$drupalExtension->addTwigPath('organisms', __DIR__ . '/patterns/03-organisms');
$drupalExtension->addTwigPath('communities', __DIR__ . '/patterns/04-communities');
$drupalExtension->addTwigPath('pages', __DIR__ . '/patterns/05-pages');

return MannequinConfig::create()
  ->addExtension($drupalExtension)
  ->setGlobalJs([
     'js/scripts.js',
  ])
  ->setGlobalCss([
     'css/compiled/style.css',
  ]);