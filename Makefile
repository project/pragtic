THEME_NAME := pragtic

clean:
	echo "node_modules\nsassdoc\ncss/compiled" > .gitignore
	rm -rf .git/
	find ./ -name "*.yml" -type f -exec sed -i '' -e 's/THEME_NAME/$(THEME_NAME)/g' {} +
	find ./ -name "*.php" -type f -exec sed -i '' -e 's/THEME_NAME/$(THEME_NAME)/g' {} +
	find ./ -name "*.twig" -type f -exec sed -i '' -e 's/THEME_NAME/$(THEME_NAME)/g' {} +
	find ./ -name "*.scss" -type f -exec sed -i '' -e 's/THEME_NAME/$(THEME_NAME)/g' {} +
	find ./ -name "*.js" -type f -exec sed -i '' -e 's/THEME_NAME/$(THEME_NAME)/g' {} +
	sed -i '' -e 's/pragtic/$(THEME_NAME)/g' package.json pragtic.breakpoints.yml pragtic.info.yml pragtic.libraries.yml pragtic.theme README.md
	mv pragtic.breakpoints.yml $(THEME_NAME).breakpoints.yml
	mv pragtic.info.yml $(THEME_NAME).info.yml
	mv pragtic.libraries.yml $(THEME_NAME).libraries.yml
	mv pragtic.theme $(THEME_NAME).theme
	mv config/install/block.block.pragtic_account_menu.yml config/install/block.block.$(THEME_NAME)_account_menu.yml
	mv config/install/block.block.pragtic_branding.yml config/install/block.block.$(THEME_NAME)_branding.yml
	mv config/install/block.block.pragtic_breadcrumbs.yml config/install/block.block.$(THEME_NAME)_breadcrumbs.yml
	mv config/install/block.block.pragtic_content.yml config/install/block.block.$(THEME_NAME)_content.yml
	mv config/install/block.block.pragtic_footer.yml config/install/block.block.$(THEME_NAME)_footer.yml
	mv config/install/block.block.pragtic_help.yml config/install/block.block.$(THEME_NAME)_help.yml
	mv config/install/block.block.pragtic_local_actions.yml config/install/block.block.$(THEME_NAME)_local_actions.yml
	mv config/install/block.block.pragtic_local_tasks.yml config/install/block.block.$(THEME_NAME)_local_tasks.yml
	mv config/install/block.block.pragtic_main_menu.yml config/install/block.block.$(THEME_NAME)_main_menu.yml
	mv config/install/block.block.pragtic_messages.yml config/install/block.block.$(THEME_NAME)_messages.yml
	mv config/install/block.block.pragtic_page_title.yml config/install/block.block.$(THEME_NAME)_page_title.yml
	mv config/install/block.block.pragtic_powered.yml config/install/block.block.$(THEME_NAME)_powered.yml
	mv config/install/block.block.pragtic_tools.yml config/install/block.block.$(THEME_NAME)_tools.yml
	mv config/optional/block.block.pragtic_search.yml config/optional/block.block.$(THEME_NAME)_search.yml
	rm Makefile