# pragtic

An atomic boilerplate of a theme focused on using Mannequin.io and SassDocs.

Developed and maintained by Metal Toad.

## First Download Steps

1. Edit `Makefile`
2. Change `pragtic` to your theme's name
3. Run `make`
4. Delete this section
5. Commit

## Sass Setup

Run `npm install`.

### Compressed Compile

Run `npm run-script compile`

### Development

Run `npm run-script watch`

See gulpfile.js for other available commands.

## Twig Structure

### Patterns

Twig templates for prototyping design are located in the `patterns/` directory.

Design/develop with the expectation that twigs will be expressing dynamic content from Drupal.
If a basic example twig is needed for Mannequin, but is not intended to be used by Drupal, name
it in the pattern: `_component.sample.twig`.

The plan for this directory is to break up templates in an atomic design fashion:

```
patterns/
  - 00-base/
    - [subcategories]
      - [component].html.twig
  - 01-atoms/
    - [subcategories]
      - [component].html.twig
  - 02-molecules/
    - [subcategories]
      - [component].html.twig
  - 03-organisms/
    - [subcategories]
      - [component].html.twig
  - 04-pages/
    - [subcategories]
      - [component].html.twig
```

Mannequin will only look for components in this directory. See the
[documentation](https://mannequin.io/extensions/drupal/components/) for building Mannequin
components. It is recommended that one starts with Drupal twig template for a component. For
instance, if the component is an atom, likely the `field.html.twig` should be used as a base.
Also, if the component is an organism, likely the `node.html.twig` should be used for that base.

### Using Patterns

Drupal will search all twig files in this entire theme for naming patterns match its template
pattern, but those should be kept exclusively in the `templates/` directory.

If design utilized Drupal templates correctly, then assigning them to specific content and the
respective fields should be fairly easy. Simply, copy the target's twig template and rename it as
per the usual Drupal theming pattern (i.e., using theme suggestions). In most cases, all of the
twig logic can be replaced with an `{% include %}`. For instance, if a content type's field is
determined to be rendered as a specific atom, then the file
`templates/[feature]/field--node--[field-name]--[content-type].html.twig` would contain:

```twig
{% include '@atoms/[subcategory]/[component].html.twig' %}
```

## Sass Structure

- `patterns/style.scss`  
  Entry point
- `patterns/_lib.scss`  
  Imported Sass libraries (Important that they do not print any CSS)
- `patterns/doc.scss`  
  Print CSS + JSON for Sass documentation
- `patterns/_sassdoc.scss`  
  Imports strictly non-CSS print base Sass in `patterns/00-base`
- `patterns/00-base/_base.scss`  
  Explicitly imports all Sass files in its directory and subdirectories  
  Any Sass added to the `/patterns/00-base` folder needs to be added to this file  
  If the Sass doesn't print any CSS, it should be added to `patterns/_sassdoc.scss`
- `patterns/01-atoms/**/*`  
- `patterns/02-molecules/**/*`  
- `patterns/03-organisms/**/*`  
- `patterns/04-communities/**/*`  
- `patterns/05-pages/**/*`  
  The above directories that are imported via globbing (i.e., `**/*`), if any mixins and such 
  are needed, then they should be placed in their own file (e.g. `_component.mixins.scss`), 
  separate from any CSS printing Sass, and explicitly added to `patterns/_sassdoc.scss`.

## Sass Documentation

_Powered by the [Herman theme for SassDoc](http://oddbird.net/herman/docs/index.html)_

View at [http://localhost/themes/custom/atomic/sassdoc/](http://localhost/themes/custom/atomic/sassdoc/)

### Herman Documentation

- [Color Palettes](http://oddbird.net/herman/docs/demo_colors.html)
  - [Accoutrement-Color](http://oddbird.net/accoutrement-color/)
- [Font Specimens](http://oddbird.net/herman/docs/demo_fonts.html)
  - [Accoutrement-Type](http://oddbird.net/accoutrement-type/)
- [Ratios & Sizes](http://oddbird.net/herman/docs/demo_sizes.html)
  - [Accoutrement-Scale](http://oddbird.net/accoutrement-scale/)
- [SVG Icons](http://oddbird.net/herman/docs/demo_icons.html)
- [Rendered Examples](http://oddbird.net/herman/docs/demo_examples.html)

## Mannequin

### View Designs

1. From this directory, where `.mannequin.php` resides, run: `npm run-script mannequin`
2. Navigate to [http://0.0.0.0:8000/](http://0.0.0.0:8000/) in your preferred browser