/*
 * Created in part with thanks to: https://github.com/markomitranic/Gulp-Sass-Starter-Kit
 * and https://github.com/oddbird/herman-test-project/
 * If you want to add Babel, see the markomitranic repo.
 */

const scssInput  = ['patterns/style.scss', 'patterns/doc.scss', 'patterns/**/*.scss'];
const scssOutput = 'css/compiled';

// Start everything up.
const gulp           = require('gulp');
const sass           = require('gulp-sass');
const autoprefixer   = require('gulp-autoprefixer');
const sourcemaps     = require('gulp-sourcemaps');
const moduleImporter = require('sass-module-importer'); // eslint-disable-line import/no-unresolved
const sassGlob       = require('gulp-sass-glob');
const sassdoc        = require('sassdoc');

// Try to ensure that all processes are killed on exit
const spawned = [];
process.on('exit', () => {
  spawned.forEach((pcs) => {
    pcs.kill();
  });
});

// Compile CSS, compressed
gulp.task('sass', () => gulp
  .src(scssInput)
  .pipe(sassGlob())
  .pipe(sourcemaps.init())
  .pipe(sass({ outputStyle: 'compressed', importer: moduleImporter() }).on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(scssOutput)));

// Compile CSS, debugging
gulp.task('dev-sass', () => gulp
  .src(scssInput)
  .pipe(sassGlob())
  .pipe(sourcemaps.init())
  .pipe(sass({ importer: moduleImporter() }).on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(scssOutput)));

// SassDoc compilation.
// See: http://sassdoc.com/customising-the-view/
gulp.task('sassdoc', () => {
  const config = {
    verbose: true,
    theme: 'herman',
    herman: {
      extraLinks: [
        {
          name: 'THEME_NAME\'s Mannequin',
          url: 'http://0.0.0.0:8000/',
        },
      ],
      sass: {
        jsonfile: `${scssOutput}/doc.css`,
        includepaths: [
          'patterns',
        ],
        includes: [
          'sassdoc',
        ],
      },
      customCSS: `${scssOutput}/style.css`,
    },
    display: {
      alias: true,
    },
    groups: {
      undefined: 'General Notes',
      scale: 'Ratios & Scales',
      fonts: 'Fonts',
      colors: 'Color Palette',
      breakpoints: 'Breakpoints',
      grid: 'Grid',
      'general-mixins': 'General Mixins',
      'helper-classes': 'Helper Classes',
      images: 'Images',
    },
  };

  const stream = sassdoc(config);

  gulp
    .src(scssInput)
    .pipe(stream);

  return stream.promise;
});

// Minified compiling task
gulp.task('compile', gulp.series('sass', 'sassdoc'));
// Development compiling task
gulp.task('dev-compile', gulp.series('dev-sass', 'sassdoc'));

// Active development watching task
gulp.task('watcher', () => {
  gulp.watch('patterns/**/*.scss', gulp.series('dev-compile'));
});
gulp.task('watch', gulp.series('dev-compile', gulp.parallel('watcher')));

// Default task
gulp.task('default', gulp.parallel('watch'));
